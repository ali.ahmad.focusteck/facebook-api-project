import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
} from '@nestjs/common';
import { FriendService } from './friend.service';
import { CreateFriendDto } from './dto/create-friend.dto';
import { UpdateFriendDto } from './dto/update-friend.dto';
import { ApiTags } from '@nestjs/swagger';

@Controller('friend')
@ApiTags('friend')
export class FriendController {
  constructor(private readonly friendService: FriendService) {}

  @Post()
  create(@Body() data: CreateFriendDto) {
    return this.friendService.create(data);
  }

  @Get()
  findAll() {
    return this.friendService.findAll();
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.friendService.findOne(+id);
  }

  @Patch(':id')
  update(@Param('id') id: string, @Body() data: any) {
    return this.friendService.update(+id, data);
  }

  @Get('mutual/:userId/:friendId/')
  updateForResponse(
    @Param('userId') userId: string,
    @Param('friendId') friendId: string,
  ) {
    return this.friendService.findMutual(userId, friendId);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.friendService.remove(+id);
  }
}
