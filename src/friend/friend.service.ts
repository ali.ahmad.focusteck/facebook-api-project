import { ConsoleLogger, Injectable, Logger } from '@nestjs/common';
import { CreateFriendDto } from './dto/create-friend.dto';
import { UpdateFriendDto } from './dto/update-friend.dto';
import { Friend, UserRequest } from './entities/friend.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository, createQueryBuilder, SimpleConsoleLogger } from 'typeorm';
import { AppDataSource } from 'src';
import { intersection } from 'lodash';
import { User } from 'src/users/entities/user.entity';

@Injectable()
export class FriendService {
  constructor(
    @InjectRepository(Friend)
    private friendRepository: Repository<Friend>,
  ) {}
  // creating a friend request
  async create(data: CreateFriendDto) {
    let task: any = new Friend();
    task.user = data.user;
    task.friend = data.friend;
    task.status = data.status;
    await this.friendRepository.save(task);
    console.log(task);
    return { message: 'All friends are inserted' };
  }

  findAll() {
    return this.friendRepository.find({
      relations: ['user'],
    });
  }

  async findMutual(userId: any, friendId: any) {
    console.log(userId, friendId);

    // find all friends of user 1
    // find all friends of user 2
    // use any union opertor to get mutual friend info

    let user: any = await AppDataSource.getRepository(Friend)
      .createQueryBuilder('fd')
      .leftJoinAndSelect('fd.friend', 'friend')
      .where('fd.user = :id', { id: userId })
      .getMany();
    let friend: any = await AppDataSource.getRepository(Friend)
      .createQueryBuilder('fd')
      .leftJoinAndSelect('fd.friend', 'friend')
      .where('fd.user = :id', { id: 2 })
      .getMany();

      user = user.map((item) => item.friend.id);
      friend = friend.map((item) => item.friend.id);

    const mutualFriend = intersection(user, friend);
    const result = await  AppDataSource.getRepository(User)
    .createQueryBuilder('user')
    .where('user.id IN (:...ids)', { ids: mutualFriend })
    .getMany()
    // const friendData = await AppDataSource.getRepository(Friend)
    //   .createQueryBuilder('friend')
    //   .where('friend.userId = :userId', { userId: friendId })
    //   .getMany();
    return result;
  }

  findOne(id: number) {
    return `This action returns a #${id} friend`;
  }

  // updating the friend request
  async update(id: number, data: any) {
    await createQueryBuilder()
      .insert()
      .update(Friend)
      .set({ status: UserRequest.APPROVED })
      .where('id = :id', { id: id })
      .execute();
    console.log(data, id);
    // const result = _.union(data,fr)
    return `This action updates a #${id} friend`;
  }

  async remove(id: number) {
    const savedTask = await this.friendRepository.findOneBy({ id: id });
    const removed = await this.friendRepository.remove(savedTask);
    console.log('removed: ', removed);

    return `This action removes a #${id} task`;
  }
}
