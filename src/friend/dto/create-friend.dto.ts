import {
  IsDate,
  IsEmail,
  IsNotEmpty,
  IsNumber,
  IsObject,
  IsString,
} from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';

export class CreateFriendDto {
  @ApiProperty()

  @IsNotEmpty()
  @IsObject()
  user:{}
  @ApiProperty()

  @IsNotEmpty()
  @IsObject()
  friend:{}
  @ApiProperty()

  @IsString()
  status:string
}
