import { User } from 'src/users/entities/user.entity';
import {
  Entity,
  Column,
  ManyToOne,
  PrimaryGeneratedColumn,
  CreateDateColumn,
  UpdateDateColumn,
  JoinColumn,
} from 'typeorm';

export enum UserRequest {
  DENIED = 'denied',
  APPROVED = 'approved',
  PEDING = 'pending',
}
@Entity()
export class Friend {
  @PrimaryGeneratedColumn()
  id: number;

//   @Column({ length: 100 })
//   firstName: string;

//   @Column({ length: 100 })
//   lastName: string;

  @Column({
    type: 'enum',
    enum: UserRequest,
    default: UserRequest.DENIED,
  })
  status

  @CreateDateColumn()
  created_at: Date;

  @UpdateDateColumn()
  updated_at: Date;

  @ManyToOne(() => User, user => user.friends)
  user: User;
  
  @ManyToOne(() => User, user => user.friends)
  friend: User;
}
