import { UsersService } from './../users/user.service';
import { Module } from '@nestjs/common';
import { FriendService } from './friend.service';
import { FriendController } from './friend.controller';
import {TypeOrmModule} from '@nestjs/typeorm'
import { Friend } from './entities/friend.entity';

@Module({
  controllers: [FriendController],
  imports: [
    TypeOrmModule.forFeature([
      Friend
    ])
  ],
  providers: [FriendService,UsersService]
})
export class FriendModule {}
