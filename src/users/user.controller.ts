import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
} from '@nestjs/common';
import { UsersService } from './user.service';
import { CreateUserDto } from './dtos/create-user.dto';
import { ApiTags } from '@nestjs/swagger';
@Controller('users')
@ApiTags('users')
export class UsersController {
  constructor(private readonly usersService: UsersService) {}

  @Post()
  create(@Body() createUserDto: CreateUserDto) {
    return this.usersService.create(createUserDto);
  }

  @Get()
  findAll() {
    return this.usersService.findAll();
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.usersService.findOne(+id);
  }
// request friend invite
  @Patch(':id')
  updateForRequest(@Param('id') id: string, @Body() data: any) {
    return this.usersService.update(+id, data);
  }
// accept friend invite
// @Patch(':userId/:friendId/')
// updateForResponse(@Param('userId') userId: string, @Param('friendId') friendId:string) {
//   return this.usersService.update(userId, friendId);
// }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.usersService.remove(+id);
  }
}
