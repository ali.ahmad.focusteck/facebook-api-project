import { Friend } from 'src/friend/entities/friend.entity';
import { Entity, Column,JoinTable,Unique,ManyToMany, PrimaryGeneratedColumn,CreateDateColumn,UpdateDateColumn, OneToMany } from 'typeorm';

@Entity()
@Unique(["email"])
export class User {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ length: 100 })
  firstName: string;

  @Column({length: 100 })
  lastName: string;

  @Column({ name:'slug', length:90})
  email: string;

  @Column()
  password:string;

  @CreateDateColumn()
  created_at: Date;

  @UpdateDateColumn()
  updated_at: Date;

  @OneToMany(()=> Friend,friend =>friend.user,{
    cascade:true
  })
  friends: Friend[];
  // @JoinTable()

}