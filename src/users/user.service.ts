import { Injectable } from '@nestjs/common';
import { AppDataSource } from 'src';
import { CreateUserDto, UpdateUserDto } from './dtos/index';
import { User } from './entities/user.entity';

const userRepository = AppDataSource.getRepository(User);
@Injectable()
export class UsersService {
  async create(CreateUserDto: any) {
    console.log(CreateUserDto, 'creates');

      let task = new User();
      task.firstName = CreateUserDto.firstName;
      task.lastName = CreateUserDto.lastName;
      task.email = CreateUserDto.email;
      task.password = CreateUserDto.password;
      await AppDataSource.manager.save(task);
      console.log("added user " ,task);
    

    return { message: 'All users are inserted' };
  }

  async findAll() {
    const savedTasks = await AppDataSource.manager.find(User);
    console.log('All Users from the db: ', savedTasks);
    return  savedTasks ;
  }

  async findOne(id: number) {
    // const data = await AppDataSource
    // .getRepository(User)
    // .createQueryBuilder("user")
    // .where('user.firstName = :firstName',{ firstName: "ali" })
    // .getOne()
    // console.log(data,"daata")
    const savedTask = await userRepository.findOneBy({ id: id });
    console.log('All photos from the db: ', savedTask);
    return savedTask;
  }

  async update(id: number, CreateUserDto: CreateUserDto) {
    const savedTask = await userRepository.findOneBy({ id: id });
    savedTask.firstName = CreateUserDto.firstName;
    savedTask.lastName = CreateUserDto.lastName;
    return `This action updates a #${id} task`;
  }

  async remove(id: number) {
    const savedTask = await userRepository.findOneBy({ id: id });
    const removed = await userRepository.remove(savedTask);
    console.log('removed: ', removed);

    return `This action removes a #${id} task`;
  }
}
