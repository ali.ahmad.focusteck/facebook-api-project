import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { User } from './users/entities/user.entity';
import { UsersModule } from './users/user.module';
import {TypeOrmModule} from '@nestjs/typeorm'
import { FriendModule } from './friend/friend.module';
import { Friend } from './friend/entities/friend.entity';

@Module({
  imports: [
    UsersModule,
    TypeOrmModule.forRoot({
      type: 'postgres',
      host: 'localhost',
      port: 5432,
      username: 'postgres',
      password: 'password',
      database: 'database4',
      entities: [User, Friend],
      synchronize: true,
    }),
    FriendModule,],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}